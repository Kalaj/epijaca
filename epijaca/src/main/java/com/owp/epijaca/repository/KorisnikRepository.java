package com.owp.epijaca.repository;

import com.owp.epijaca.model.Korisnik;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class KorisnikRepository {
    private static final String PUTANJA = System.getProperty("user.dir") + "/podaci/korisnici.txt";

    public Korisnik findOneByUsername(String username) {
        List<Korisnik> korisnici = findAllKorisnici();
        for (Korisnik k : korisnici) {
            if (k.getUsername().equals(username))
                return k;
        }
        return null;
    }

    public List<Korisnik> findAllKorisnici() {
        List<Korisnik> korisnici = new ArrayList<>();
        String linija;
        try (BufferedReader reader = new BufferedReader(new FileReader(PUTANJA))) {
            while ((linija = reader.readLine()) != null) {
                String[] split = linija.split(",");
                Korisnik korisnik = new Korisnik();
                korisnik.setUsername(split[0]);
                korisnik.setPassword(split[1]);
                korisnik.setImeIPrezime(split[2]);
                korisnik.setAdmin(Boolean.parseBoolean(split[3]));
                korisnici.add(korisnik);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return korisnici;
    }

}
