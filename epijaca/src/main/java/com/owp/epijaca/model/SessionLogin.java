package com.owp.epijaca.model;

public class SessionLogin {
    private final String username;
    private final boolean isAdmin;

    public SessionLogin(String username, boolean isAdmin) {
        this.username = username;
        this.isAdmin = isAdmin;
    }


    public String getUsername() {
        return username;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    @Override
    public String toString() {
        return "Korisnik " + username + " je admin " + isAdmin;
    }
}
