package com.owp.epijaca.model;

public class Prijava {
    private String username;
    private String password;

    public Prijava()
    {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
