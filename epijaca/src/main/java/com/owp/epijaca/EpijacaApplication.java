package com.owp.epijaca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpijacaApplication {

    public static void main(String[] args) {
        SpringApplication.run(EpijacaApplication.class, args);
    }

}
