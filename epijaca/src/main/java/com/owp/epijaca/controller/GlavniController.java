package com.owp.epijaca.controller;

import com.owp.epijaca.model.SessionLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("/")
public class GlavniController {
    @Autowired
    private HttpSession session;

    @GetMapping("/provera-tvoja")
    @ResponseBody 
    public String get(HttpServletResponse res) throws IOException {
        // validacija da li je korisnik ulogovan, svuda mora da ide jer sistem ne mogu da koriste neulogovani korisnici
        SessionLogin sessionLoginKorisnika = (SessionLogin) session.getAttribute("sessionLogin");
        if (sessionLoginKorisnika == null)
        {
            res.sendRedirect("/prijava-odjava");
            return "";
        }

        String html = " Perspektiva admina. Ja sam admin: ";
        html+=sessionLoginKorisnika.getUsername() + ".";

        if (sessionLoginKorisnika.isAdmin())
            html+="";
        else
            html+=" Perspektiva korisnika.";

        return html;
    }

}
