package com.owp.epijaca.controller;

import com.owp.epijaca.model.Korisnik;
import com.owp.epijaca.model.Prijava;
import com.owp.epijaca.model.SessionLogin;
import com.owp.epijaca.repository.KorisnikRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("/prijava-odjava")
public class PrijavaOdjavaController {

    @Autowired
    private HttpSession session;

    @Autowired
    private KorisnikRepository korisnikRepository;

    @GetMapping
    @ResponseBody
    public String get(HttpServletResponse res) throws IOException {
        SessionLogin s = (SessionLogin) session.getAttribute("sessionLogin");
        if (s != null) {
            res.sendRedirect("/");
            return "";
        }

        String html;
        html = "<html>" +
                "<head>" +
                "<meta charset=\"UTF-8\" />" +
                "</head>" +
                "<body>" +
                "<form action=\"prijava-odjava/prijavi-se\" method=\"post\">" +
                "<input type=\"text\" name=\"username\" />" +
                "<input type=\"password\" name=\"password\" />" +
                "<input type=\"submit\" value=\"POSALJI\"/>" +
                "</form>" +
                "</body>" +
                "</html>";
        return html;
    }

    @PostMapping("/prijavi-se")
    public void prijava(@ModelAttribute Prijava prijava, HttpServletResponse res) throws IOException {
        SessionLogin sessionLoginIzSesije = (SessionLogin) session.getAttribute("sessionLogin");
        if (sessionLoginIzSesije != null) {
            res.sendRedirect("/");
            return;
        }
        // sessionLogin ne postoji, korisnik nije ulogovan
        Korisnik korisnik = korisnikRepository.findOneByUsername(prijava.getUsername());
        if (korisnik == null) {
            res.sendRedirect("/prijava-odjava");
            return;
        }
        if (!korisnik.getPassword().equals(prijava.getPassword())) {
            res.sendRedirect("/prijava-odjava");
            return;
        }
        //korisnik postoji, mozemo da pravimo sessionLogin

        SessionLogin sessionLoginNovi = new SessionLogin(korisnik.getUsername(), korisnik.isAdmin());
        System.out.println(sessionLoginNovi);
        session.setAttribute("sessionLogin", sessionLoginNovi);
        res.sendRedirect("/");
    }

    @GetMapping("/odjavi-se")
    public void odjava(HttpServletResponse res) throws IOException {
        SessionLogin sessionLoginIzSesije = (SessionLogin) session.getAttribute("sessionLogin");
        if (sessionLoginIzSesije == null) {
            res.sendRedirect("/prijava-odjava");
            return;
        }
        // korisnik je ulogovan

        session.removeAttribute("sessionLogin");
        res.sendRedirect("/prijava-odjava");
    }

}
